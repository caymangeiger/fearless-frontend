import {useState, useEffect} from 'react';


function LocationList() {
    const [locations, setLocations] = useState([]);
    const [sortDirection, setSortDirection] = useState("up");
    const [states, setStates] = useState([]);
    const [state, setState] = useState("");


    async function loadLocations() {
    const response = await fetch("http://localhost:8000/api/locations/");
    const data = await response.json();
    setLocations(data.locations);
}
    async function fetchData() {
    const response =  await fetch ('http://localhost:8000/api/states/');
    const data = await response.json();
    setStates(data.states);

    }
    useEffect(() => {
        fetchData();
    }, []);


    async function filterLocation(){
        const response = await fetch("http://localhost:8000/api/locations/");
        const data = await response.json();
        setLocations(data.locations.filter(location => {
                return location.state.abbreviation === state
            }));
    }

    function handleSortChange(e){
        setSortDirection(e.target.value)
    }

    const handleStateChange = (event) => {
    const value = event.target.value
    setState(value);
    }
    useEffect(() => {
        filterLocation();
    }, [state]);
    useEffect(() => {
        loadLocations()
    }, []);


    async function handleOnDelete() {
        const request = await fetch ("http://localhost:8000/api/locations/", {
            method: "DELETE"
        });

        const resp = await request.json()
        if (resp.deleted){
            loadLocations()
        } else {
            alert("Not Deleted!")
        }
    }


    const sortedList = locations.sort((locationA, locationB) => {
        if (sortDirection === "up"){
            return (locationA.id - locationB.id)
        } else {
            return (locationB.id - locationA.id)
        }
    })


return (
    <>
    <select onChange={handleSortChange}>
        <option value="up">Up</option>
        <option value="down">Down</option>
    </select>
    <select value={state} onChange={handleStateChange} required name="state" id="state">
        <option value="" >Choose a state</option>
        {states.map(state => {
            return (
            <option key={state.abbreviation} value={state.abbreviation} >
            {state.name}
            </option>
            );
        })}
    </select>

<table className="table table-striped">

    <thead>
    <tr>
        <th>ID</th>
        <th>Names</th>
        <th>States</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    {sortedList.map(location => {
        return (
        <tr key={location.id}>
            <td>{ location.id }</td>
            <td>{ location.name }</td>
            <td>{ location.state.name }</td>
            <td><button onClick={handleOnDelete} id={location.id}className="btn btn-danger">Delete</button></td>
        </tr>
        );
    })}
    </tbody>
</table>
</>
);
}
export default LocationList;
