import { NavLink } from 'react-router-dom';
import './styles.css';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid" >
                <a className="navbar-brand" href="/">Conference GO!</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <div className="grid m-3 d-flex">
                        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="attendeesDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Attendees
            </button>
            <ul className="dropdown-menu" aria-labelledby="attendeesDropdown">
                <li><NavLink className="dropdown-item" to="attendee">Attendees</NavLink></li>
                <li><NavLink className="dropdown-item" to="/attendee/create">Create Attendee</NavLink></li>
            </ul>
        </div>
        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="conferencesDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Conferences
            </button>
            <ul className="dropdown-menu" aria-labelledby="conferencesDropdown">
                <li><NavLink className="dropdown-item" to="/conference">Conferences</NavLink></li>
                <li><NavLink className="dropdown-item" to="/conference/create">Create Conference</NavLink></li>
            </ul>
        </div>
        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="locationsDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Locations
            </button>
            <ul className="dropdown-menu" aria-labelledby="locationsDropdown">
                <li><NavLink className="dropdown-item" to="/location">Locations</NavLink></li>
                <li><NavLink className="dropdown-item" to="/location/create">Create Location</NavLink></li>
            </ul>
        </div>
        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="presentationsDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                Presentations
            </button>
            <ul className="dropdown-menu" aria-labelledby="presentationsDropdown">
                <li><NavLink className="dropdown-item" to="/presentation">Presentations</NavLink></li>
                <li><NavLink className="dropdown-item" to="/presentation/create">Create Presentation</NavLink></li>
            </ul>
        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default Nav;
