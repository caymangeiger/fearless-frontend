import {useState, useEffect} from 'react';

function AttendeeList() {
    const [attendees, setAttendees] = useState([]);
    const [sortDirection, setSortDirection] = useState("up");
    const [conferences, setConferences] = useState([]);
    const [state, setState] = useState("");


    async function loadAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');
    if (response.ok) {
    const data = await response.json();
    setAttendees(data.attendees);
    }}

    // async function fetchData() {
    // const response =  await fetch ('http://localhost:8000/api/conferences/');
    // const data = await response.json();
    // setStates(data.states);
    // }
    useEffect(() => {
        loadAttendees()
        // fetchData();
    }, []);


    // const handleConferenceChange = (event) => {
    // const value = event.target.value
    // setState(value);
    // }


    // useEffect(() => {
    // loadAttendees();
    // }, [state]);


return (
    <>
    {/* <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference">
        <option>Choose a conference</option>
        {conferences.map(conference => {
            return (
            <option key={conference.id} value={conference.id} >
            {conference.name}
            </option>
            );
        })}
    </select> */}
<table className="table table-striped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Conference</th>
    </tr>
    </thead>
    <tbody>
    {attendees.map(attendee => {
        return (
        <tr key={attendee.href}>
            <td>{ attendee.name }</td>
            <td>{ attendee.conference }</td>
        </tr>
        );
    })}
    </tbody>
</table>
</>
);
}

export default AttendeeList;
