import {useState, useEffect} from 'react';

function PresentationList() {
    const [presentations, setPresentations] = useState([]);
    const [sortDirection, setSortDirection] = useState("up");
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState(20);



    async function loadPresentations() {
    const pk = conference
    const response = await fetch(`http://localhost:8000/api/conferences/${pk}/presentations/`);
    const data = await response.json();
    setPresentations(data.presentations);

    }
    async function fetchData() {
    const response =  await fetch ('http://localhost:8000/api/conferences/');
    const data = await response.json();
    setConferences(data.conferences);
    }
    useEffect(() => {
        fetchData();
        loadPresentations()
    }, []);

    // async function presentationList(){
    //     const response =  await fetch ('http://localhost:8000/api/conferences/');
    //     const data = await response.json();
    //     for (let conferenceId in data.conferences.id){
    //         if (conferenceId === undefined){
    //             continue;
    //         } else {
    //         let pk = conference.id
    //         console.log(pk)
    //         const presResponse =  await fetch (`http://localhost:8000/api/conferences/${pk}/presentations/`);
    //         const presData = await presResponse.json();
    //         setPresentations(presData)
    //         pk = "0"
    //     }}
    // }

    function handleSortChange(e){
        setSortDirection(e.target.value)
    }

    const handleConferenceChange = (event) => {
    const value = event.target.value
    setConference(value);
    }
    useEffect(() => {
    loadPresentations();
    }, [conference]);


    // const handleAllConferences = () => {
    // presentationList()
    // }



    async function handleOnDelete(e) {
        const pk = e.target.id
        const request = await fetch (`http://localhost:8000/api/presentations/${pk}/`, {
            method: "DELETE"
        });

        const resp = await request.json()
        if (resp.deleted){
            loadPresentations()
        } else {
            alert("Not Deleted!")
        }
    }


    const sortedList = presentations.sort((presentationA, presentationB) => {
        if (sortDirection === "up"){
            return (presentationA.id - presentationB.id)
        } else {
            return (presentationB.id - presentationA.id)
        }
    })


return (
    <>
    <select onChange={handleSortChange}>
        <option value="up">Up</option>
        <option value="down">Down</option>
    </select>
    <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference">
        <option>Choose a conference</option>
        {conferences.map(conference => {
            return (
            <option key={conference.id} value={conference.id} >
            {conference.name}
            </option>
            );
        })}
    </select>
    {/* <button onClick={handleAllConferences}>All Conferences</button> */}

<table className="table table-striped">

    <thead>
    <tr>
        <th>ID</th>
        <th>Titles</th>
        <th>Conferences</th>
        <th>Locations</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    {sortedList.map(presentation => {
        return (
        <tr key={presentation.id}>
            <td>{ presentation.id }</td>
            <td>{ presentation.title }</td>
            <td>{ presentation.conference.name }</td>
            <td>{ presentation.conference.location.name }</td>
            <td><button onClick={handleOnDelete} id={presentation.id}className="btn btn-danger">Delete</button></td>
        </tr>
        );
    })}
    </tbody>
</table>
</>
);
}
export default PresentationList;
