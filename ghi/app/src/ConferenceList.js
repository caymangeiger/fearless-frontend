import {useState, useEffect} from 'react';

function ConferenceList() {
    const [conferences, setConferences] = useState([]);
    const [sortDirection, setSortDirection] = useState("up");

    async function loadConferences() {
    const response = await fetch('http://localhost:8000/api/conferences/');
    const data = await response.json();
    setConferences(data.conferences);
    }
    useEffect(() => {
        loadConferences();
    }, []);

    function handleSortChange(e){
        setSortDirection(e.target.value)
    }
    // async function handleClick(e) {
    //     const request = await fetch (`http://localhost:8000/api/conferences/${e.target.id}/`, {
    //         method: "DELETE"
    //     });

    //     const resp = await request.json
    // }
    const sortedList = conferences.sort((conferenceA, conferenceB) => {
        if (sortDirection === "up"){
            return (conferenceA.id - conferenceB.id)
        } else {
            return (conferenceB.id - conferenceA.id)
        }
    })

return (
<table className="table table-striped">
    <select onChange={handleSortChange}>
        <option value="up">Up</option>
        <option value="down">Down</option>
    </select>

    <thead>
    <tr>
        <th>IDs</th>
        <th>Names</th>
        <th>Locations</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    {sortedList.map(conference => {
        return (
        <tr key={conference.id}>
            <td>{ conference.id }</td>
            <td>{ conference.name }</td>
            <td>{ conference.location.name }</td>
            <td><button id={conference.id}className="btn">Delete</button></td>
        </tr>
        );
    })}
    </tbody>
</table>
);
}
export default ConferenceList;
