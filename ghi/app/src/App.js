import Nav from './Nav';
import AttendeeConferenceForm from './AttendeeConferenceForm';
import AttendeeList from './AttendeeList';
import LocationForm from './LocationForm';
import LocationList from './LocationList';
import ConferenceForm from './ConferenceForm';
import ConferenceList from './ConferenceList';
import PresentationForm from './PresentationForm';
import PresentationList from './PresentationList';
import MainPage from './MainPage';
import { Route, Routes} from 'react-router-dom';


function App() {
  return (
    <div className='App'>
      <Nav/>
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="location">
          <Route index element={<LocationList/>} />
          <Route path="create" element={<LocationForm/>} />
        </Route>
        <Route path="conference">
          <Route index element={<ConferenceList/>} />
          <Route path="create" element={<ConferenceForm/>} />
        </Route>
        <Route path="presentation">
          <Route index element={<PresentationList/>} />
          <Route path="create" element={<PresentationForm/>} />
        </Route>
        <Route path="attendee">
          <Route index element={<AttendeeList/>} />
          <Route path="create" element={<AttendeeConferenceForm/>} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
